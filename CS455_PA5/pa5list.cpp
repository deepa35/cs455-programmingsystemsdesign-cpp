// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CS 455 PA5

// pa5list.cpp
// a program to test the linked list code necessary for a hash table chain

// You are not required to submit this program for pa5.

// We gave you this starter file for it so you don't have to figure
// out the #include stuff.  The code that's being tested will be in
// listFuncs.cpp, which uses the header file listFuncs.h

// The pa5 Makefile includes a rule that compiles these two modules
// into one executable.

#include <iostream>
#include <string>
#include <cassert>

using namespace std;

#include "listFuncs.h"

void testAdd(ListType& list) {
	addNode(list, "Mary", 30);
	addNode(list, "Sue", 32);
	addNode(list, "Dan", 28);
	addNode(list, "Jenna", 35);
	addNode(list, "Chris", 40);
}

void testDelete(ListType& list) {
	cout << deleteNode(list, "Mary") << endl;
	cout << deleteNode(list, "Sue") << endl;
	cout << deleteNode(list, "Jenna") << endl;
	cout << deleteNode(list, "Chris") << endl;
}

int main() {
	ListType list;
	createList(list);
	cout << "Empty list: " << endl;
	print(list);
	cout << endl;

	//Adding elements
	cout<<"Adding all"<< endl;
	testAdd(list);
	print(list);
	cout << endl;

	cout << "Removing friends"<< endl;
	testDelete(list);
	cout << endl;
	print(list);
	cout << endl;

	cout <<"The size is :" << getSize(list)<< endl;
	cout << endl;

	cout << "Dan's marks are: " << *lookUp(list, "Dan") << endl;
	return 0;
}
