// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CSCI 455 PA5
// Spring 2018

// Table.cpp  Table class implementation


#include "Table.h"

#include <iostream>
#include <string>
#include <cassert>

// listFuncs.h has the definition of Node and its methods.  -- when
// you complete it it will also have the function prototypes for your
// list functions.  With this #include, you can use Node type (and
// Node*, and ListType), and call those list functions from inside
// your Table methods, below.

#include "listFuncs.h"

//*************************************************************************

Table::Table() {
  table = new ListType[HASH_SIZE]();
  hashSize = HASH_SIZE;
}

Table::Table(unsigned int hSize) {
  table = new ListType[hSize]();
  hashSize = hSize;
}

int * Table::lookup(const string &key) {
  return lookUp(table[hashCode(key)], key);
}

bool Table::remove(const string &key) {
  return deleteNode(table[hashCode(key)], key);
}

bool Table::insert(const string &key, int value) {
  //inserting at the beginning, so checking if the element is already present
  if(lookUp(table[hashCode(key)], key) != NULL) {
    return false;
  } else {
    addNode(table[hashCode(key)], key, value);
    return true;
  }
}

int Table::numEntries() const {
  int numEntry = 0;
  for(int count=0; count<hashSize; count++) {
    numEntry += getSize(table[count]);
  }
  return numEntry;
}

void Table::printAll() const {
  for(int count=0; count<hashSize; count++) {
    print(table[count]);
  }
}

void Table::hashStats(ostream &out) const {
  int filledBuckets = 0;
  int longestList = 0;
  for(int count=0; count<hashSize; count++) {
    int length = getSize(table[count]);
    if(length>0) {
      filledBuckets ++;
      if(length > longestList) {
	longestList = length;
      }
    }
  }
  out << "number of buckets: " << hashSize << endl;
  out << "number of entries: " << Table::numEntries() << endl;
  out << "number of non-empty buckets: " << filledBuckets << endl;
  out << "longest chain: " << longestList << endl;
}
