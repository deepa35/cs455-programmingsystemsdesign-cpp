// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CSCI 455 PA5
// Spring 2018

/*
 * grades.cpp
 * A program to test the Table class.
 * How to run it:
 *      grades [hashSize]
 * 
 * the optional argument hashSize is the size of hash table to use.
 * if it's not given, the program uses default size (Table::HASH_SIZE)
 *
 */

#include "Table.h"
// cstdlib needed for call to atoi
#include <cstdlib>

//Prints each command, format and message for user reference
void helpOutput() {
  cout << "Commands supported." << endl;
  cout << "insert: Inserts student name and score into the table (Format: insert name score)." << endl;
  cout << "change: Updates the score for a particular student (Format: change name score)." << endl;
  cout << "lookup: Search and return score of a given student (Format: lookup name)."	<< endl;
  cout << "remove: Removes the student entry from table given student name (Format: remove name)." << endl;
  cout << "print: Prints out all the entries in the table (Format: print)." << endl;
  cout << "size: Prints the number of entries in the table (Format: size)." << endl;
  cout << "stats: Prints the hash table statistics (Format: stats)." << endl;
  cout << "help: Prints out a brief summary of the commands (Format: help)." << endl;
  cout << "quit: Exits the program (Format: quit)." << endl;
}

//Inserts the student name and grade into the table if the entry doesn't exist
void insert(Table* grades) {
  string name;
  cin >> name;
  string grade;
  cin >> grade;
  bool insertStatus = grades->insert(name, atoi(grade.c_str()));
  if (insertStatus) {
    cout << "Student name and score successfully inserted." << endl;
  } else {
    cout << "Student details already exist in table. Please check again." << endl;
  }
}

//Modifies the grade of student if the entry exists
void change(Table* grades) {
  string name;
  cin >> name;
  string grade;
  cin >> grade;
  int* lookUp = grades->lookup(name);
  if (lookUp == NULL) {
    cout << "Student " << name << " does not exist in table. Change aborted. Try again."	<< endl;
  } else {
    *lookUp = atoi(grade.c_str());
    cout << "Student score updated successfully." << endl;
  }
}

//Looks up grade of a student by name
void lookUp(Table* grades) {
  string name;
  cin >> name;
  int* lookUp = grades->lookup(name);
  if (lookUp == NULL) {
    cout << "Student not found in table." << endl;
  } else {
    cout << "Student " << name << " found. The score is " << *lookUp << "." << endl;
  }
}

//Tries to remove the entry associated with the given student name
void remove(Table* grades) {
  string name;
  cin >> name;
  bool removeStatus = grades->remove(name);
  if (removeStatus) {
    cout << "Student " << name << " successfully removed." << endl;
  } else {
    cout << "Student entry is not present in table. Deletion aborted." << endl;
  }
}

//Reads the command given by user and executes the appropriate function
void execute(Table* grades) {
  bool continueExecution = true;
  while (continueExecution) {
    string option;
    cout << "cmd> ";
    cin >> option;
    if (option == "insert") {
      insert(grades);
    } else if (option == "change") {
      change(grades);
    } else if (option == "lookup") {
      lookUp(grades);
    } else if (option == "remove") {
      remove(grades);
    } else if (option == "print") {
      cout << "All the entries in the table are as follows." << endl;
      grades->printAll();
    } else if (option == "size") {
      cout << "The number of entries in the table are: " << grades->numEntries() << "." << endl;
    } else if (option == "stats") {
      cout << "Table statistics." << endl;
      grades->hashStats(cout);
    } else if (option == "help") {
      helpOutput();
    } else if (option == "quit") {
      continueExecution = false;
    } else {
      cout << "ERROR:invalid command" << endl;
      cin.clear();
      cin.ignore(INT_MAX,'\n');
      helpOutput();
    }
  }
}

//Main method that starts the execution
//Creates a hashtable of given/default size, prints the statistics
//Provides various options to user to create and access a customised hashtable
int main(int argc, char * argv[]) {
  // gets the hash table size from the command line
  int hashSize = Table::HASH_SIZE;
  Table * grades;  // Table is dynamically allocated below, so we can call
  // different constructors depending on input from the user.
  if (argc > 1) {
    hashSize = atoi(argv[1]);  // atoi converts c-string to int
    if (hashSize < 1) {
      cout << "Command line argument (hashSize) must be a positive number"<< endl;
      return 1;
    }
    grades = new Table(hashSize);
  }
  else {   // no command line args given -- use default table size
    grades = new Table();
  }
  grades->hashStats(cout);
  execute(grades);
  return 0;
}
