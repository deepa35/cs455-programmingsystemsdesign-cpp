// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CSCI 455 PA5
// Spring 2018


#include <iostream>
#include <cassert>
#include "listFuncs.h"

using namespace std;

Node::Node(const string &theKey, int theValue) {
  key = theKey;
  value = theValue;
  next = NULL;
}

Node::Node(const string &theKey, int theValue, Node *n) {
  key = theKey;
  value = theValue;
  next = n;
}


//*************************************************************************
// put the function definitions for your list functions below

void createList(ListType& myList) {
  myList = NULL;
}

int* lookUp(ListType myList, const string &key) {
  Node *node = myList;
  while(node != NULL) {
    if(node->key == key) {
      return &(node->value);
    }
    node = node->next;
  }
  return NULL;
}

void addNode(ListType& myList, const string &key, int value) {
  Node *newNode = new Node(key,value);
  newNode->next = myList;
  myList = newNode;
}

bool deleteNode(ListType& myList, const string &key) {
  Node *node = myList;
  Node *prevNode = NULL;

  //If the first node is to be deleted
  if(node != NULL && node->key == key) {
    myList = node->next;
    delete node;
    return true;
  }

  //For all others defining a prevNode to point 
  //to previous node
  while(node != NULL && node->key != key) {
    prevNode = node;
    node = node->next;
  }

  if(node == NULL) {
    return false;
  }

  prevNode->next = node->next;
  delete node;
  return true;
}

void print(ListType myList) {
  Node *node = myList;
  while(node != NULL) {
    cout << node->key << " " << node->value << endl;
    node = node->next;
  }
}

int getSize(ListType myList) {
  int size = 0;
  Node *node = myList;
  while(node!=NULL) {
    size++;
    node = node->next;
  }
  return size;
}
