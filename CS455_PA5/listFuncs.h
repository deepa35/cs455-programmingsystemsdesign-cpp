// Name: Deepa Sreekumar
// USC NetID: dsreekum
// CSCI 455 PA5
// Spring 2018


//*************************************************************************
// Node class definition 
// and declarations for functions on ListType

// Note: we don't need Node in Table.h
// because it's used by the Table class; not by any Table client code.


#ifndef LIST_FUNCS_H
#define LIST_FUNCS_H
  
using namespace std;

struct Node {
  string key;
  int value;

  Node *next;

  Node(const string &theKey, int theValue);

  Node(const string &theKey, int theValue, Node *n);
};


typedef Node * ListType;

//*************************************************************************

//Creates an empty linked list
void createList(ListType& myList);

//Creates a node of given key, value and 
//adds it to the front of the list
void addNode(ListType& myList, const string &key, int value);

//Deletes a node of given key, if the node exists in the list
//Returns true if the node is deleted and false if node doesnt 
//exist to delete 
bool deleteNode(ListType& myList, const string &key);

//Prints elements in the list
void print(ListType myList);

//Returns size of the list
int getSize(ListType myList);

//Looks up the key and returns address of the value
//If key is not present in the table, returns NULL
int* lookUp(ListType myList,const string &key);

// keep the following line at the end of the file
#endif
