/*  Name: Deepa Sreekumar
 *  USC NetID: dsreekum
 *  CS 455 Spring 2018, Extra Credit assignment
 *
 *  See ecListFuncs.h for specification of each function.
 */

#include <iostream>
#include <cassert>
#include "ecListFuncs.h"

using namespace std;

bool isInOrder(ListType list) {
  Node * node = list;
  if(node == NULL) {
    return true;
  }
  while(node->next != NULL) {
    if(node->data > node->next->data) {
      return false;
    }
    node = node ->next;
  }
  return true;
}

void insertInOrder(ListType & list, Node *itemP) {
  // checks the preconditions
  assert(isInOrder(list));
  assert(itemP->next == NULL);
    
  Node * node = list;
  Node * prevNode = NULL;
  //insert at front
  if(node == NULL || itemP->data <= node->data) {
    itemP->next = node;
    list = itemP;
    return;
  }
  while(node != NULL && node->data <= itemP-> data) {
    prevNode = node;
    node = node -> next;
  }
  prevNode->next = itemP;
  itemP->next = node;
}

void insertionSort(ListType &list) {
  Node * sortedList = NULL;
  Node * node = list;
  Node * nextNode = NULL;
  while(node!= NULL) {
    nextNode = node->next;
    node->next = NULL;
    insertInOrder(sortedList, node);
    node = nextNode;
  }
  list = sortedList;
  assert(isInOrder(list));
}

void splitEvenOdd(ListType &list, ListType &a, ListType &b){
  Node * node = list;
  Node * tempNode = NULL;
  Node * aTail = NULL;
  Node * bTail = NULL;
  if(node!=NULL) {
    a = node;
    aTail = a;
    if(node->next!=NULL) {
      b = node->next;
      bTail = b;
      tempNode = node->next->next;
      node->next->next = NULL;
    }
    node->next = NULL;
  }
  node = tempNode;
    
  while(node!=NULL) {
    aTail->next = node;
    aTail =node;
    node = node->next;
    aTail->next = NULL;
    if(node!=NULL) {
      bTail->next = node;
      bTail =node;
      node = node->next;
      bTail->next = NULL;
    }
  }
  list = node;
}
